<?php

namespace Drupal\lightning_media_facebook\Plugin\media\Source;

use Drupal\lightning_media\InputMatchInterface;
use Drupal\lightning_media\ValidationConstraintMatchTrait;
use Drupal\media_entity_facebook\Plugin\media\Source\Facebook as BaseFacebook;

/**
 * Input-matching version of the Facebook media type.
 */
class Facebook extends BaseFacebook implements InputMatchInterface {

  use ValidationConstraintMatchTrait;

}
